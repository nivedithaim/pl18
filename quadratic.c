#include <stdio.h>
#include<math.h>
int main()
{
    int a,b,c,ch;
    float D,deno,root1,root2;
    printf("Enter the values of a,b and c\n");
    scanf("%d%d%d",&a,&b,&c);
    D=(b*b)-(4*a*c);
    deno=2*a;
    if(D>0)
    {
        ch=1;
    }
    else if(D==0)
    {
        ch=2;
    }
    else
    {
        ch=3;
    }
    switch(ch)
    {
        case 1:root1=(-b+sqrt(D))/deno;
               root2=(-b-sqrt(D))/deno;
               printf("ROOT1=%f\nROOT2=%f\n",root1,root2);
               printf("The roots are real and distinct\n");
               break;
        case 2:root1=-b/deno;
               root2=-b/deno;
               printf("ROOT1=%f\nROOT2=%f\n",root1,root2);
               printf("The roots are real and equal\n");
               break;
        case 3:root1=-b/deno;
               root2=sqrt(D)/deno;
               printf("The roots are imaginary\n");
               break;
        default:printf("Invalid Choice\n");
    }
    return 0;
}

