#include<stdio.h> 
int input()
{
    int n;
    printf("Enter a decimal number\n");
    scanf("%d",&n);
    return n;
}

int convert(int a)
{
    int n=0,bin=0;
    int b[1000];
    for(int i=0;a>0;i++,n++)
    {
        b[i]=a%2;
        a=a/2;
    }
    for(int i=n-1;i>=0;i--)
    {
        bin=(bin*10)+b[i];
    }
    return bin;
}
void display(int num, int result)
{
    printf("The binary form of %d is %d",num,result);
}

int main()
{
    int a=input();
    int c=convert(a);
    display(a,c);
    return 0;
}