#include<stdio.h>
int main()
{
    int n;
    printf("Enter the number of elements\n");
    scanf("%d",&n);
    int a[n];
    printf("Enter %d numbers\n",n);
    
    for(int i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    

    int num, pos;
    printf("Enter the number to be inserted and its position respectively\n");
    scanf("%d %d",&num,&pos);
    
    for(int i=n;i>pos;i--)
    {
        a[i]=a[i-1];
    }
    a[pos]=num;
    
    printf("The new array is\n");
    for(int i=0;i<=n;i++)
    {
        printf("%d  ",a[i]);
    }
    printf("\n");
    
    return 0;
}