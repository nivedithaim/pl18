#include <stdio.h>

int main()
{
    struct employee
    {
        int id;
        char name[20];
        double salary;
        char DOJ[30];
    };
    struct employee E;
    printf("Enter the name of the employee\n");
    scanf("%s",E.name);
    printf("Enter the employee ID\n");
    scanf("%d",&E.id);
    printf("Enter the salary of the employee\n");
    scanf("%lf",&E.salary);
    printf("Enter the date of joining of the employee\n");
    scanf("%s",E.DOJ);
    printf("NAME OF THE EMPLOYEE:%s\n",E.name);
    printf("EMPLOYEE ID:%d\n",E.id);
    printf("SALARY OF THE EMPLOYEE:%lf\n",E.salary);
    printf("DATE OF JOINING OF THE EMPLOYEE:%s\n",E.DOJ);
	return 0;
}
