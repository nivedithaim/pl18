#include <stdio.h>
int main()
{
    int n, m;
    printf("Enter the number of students and the number of subjects\n");
    scanf("%d %d",&n,&m);
    
    int a[n][m];
    for(int i=0;i<n;i++)
    {
        printf("Enter the marks of STUDENT %d in %d subjects\n",(i+1),m);
        for(int j=0;j<m;j++)
        {
            scanf("%d",&a[i][j]);
        }
    }
    
    for(int i=0;i<m;i++)
    {
        int highest=a[0][i];
        for(int j=0;j<n;j++)
        {
            if(a[j][i]>highest)
            highest =a[j][i];
        }
        printf("The highest marks in SUBJECT %d is %d\n",(1+i),highest);
    }
    return 0;
}

